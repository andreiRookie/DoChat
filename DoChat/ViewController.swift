//
//  ViewController.swift
//  DoChat
//
//  Created by Andrei on 24.03.2022.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        printStateLogging(firstState: "viewDisappeared/Diappearing", secondState: "viewAppearing")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        printStateLogging(firstState: "viewAppearing", secondState: "viewAppeared")
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        printStateLogging(firstState: "viewAppearing/Appeared", secondState: "viewDisappearing")
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        printStateLogging(firstState: "viewDisappearing", secondState: "viewDisappeared")
    }
    
    
    //default implementetion of these methods does nothing
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        if isLogging { print(#function) }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if isLogging { print(#function) }
    }


}

