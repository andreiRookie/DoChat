//
//  AppDelegate.swift
//  DoChat
//
//  Created by Andrei on 24.03.2022.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {



    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        printStateLogging(firstState: "Not-Running", secondState: "Foreground-INACTIVE")
        return true
    }

    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }


}

public var isLogging = true

//public var printStateLogging = { (firstState: String, secondState: String, currentFunc: String) -> Void in
//    if isLogging {
//    print("App moved from <\(firstState) state> to <\(secondState) state>: \(currentFunc)")
//    }
//}
    
func printStateLogging(firstState: String, secondState: String, currentFunc: String = #function) -> Void {
    if isLogging {
    print("App moved from <\(firstState) state> to <\(secondState) state>: \(currentFunc)")
    }
    
}

